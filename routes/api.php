<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WCController;
use App\Http\Controllers\HolooController;
use App\Http\Controllers\AuthenticationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthenticationController::class, 'register']);
Route::post('login', [AuthenticationController::class, 'login']);
Route::get('wcall', [WCController::class, 'fetchAllWCProducts']);
Route::get('wc/{id}', [WCController::class, 'fetchSingleProduct']);
Route::post('wcadd', [WCController::class, 'createSingleProduct']);
Route::post('getProductConflict', [WCController::class, 'compareProductsFromWoocommerceToHoloo']);

Route::post('updateWCSingleProduct', [WCController::class, 'updateWCSingleProduct']);
Route::post('updateAllProductFromHolooToWC', [WCController::class, 'updateAllProductFromHolooToWC']);

Route::post('getProductCategory', [HolooController::class, 'getProductCategory']);
Route::get('getAllHolooProducts', [HolooController::class, 'getAllHolooProducts']);

//woocomrece webhook event
Route::post('wcInvoiceRegistration', [HolooController::class, 'wcInvoiceRegistration']);


Route::middleware('auth:sanctum')->group(function () {
    Route::get('user', [AuthenticationController::class, 'user']);
    Route::post('updateUser', [AuthenticationController::class, 'updateWordpressSettings']);
//    Route::post('logout', [AuthenticationController::class, 'logout']);
});

//Route::get()

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
